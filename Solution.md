## Solution
```sql
select P.NAME as 'PROFESSOR.NAME', C.NAME as 'COURSE.NAME' from SCHEDULE S
left join PROFESSOR P on P.ID = S.PROFESSOR_ID
left join COURSE C on C.ID = S.COURSE_ID
where P.DEPARTMENT_ID != C.DEPARTMENT_ID
group by P.NAME, C.NAME;
```